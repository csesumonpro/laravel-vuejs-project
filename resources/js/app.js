/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// user custom js file
import Gate from "./custom";
Vue.prototype.$gate = new Gate(window.user);

// Use Progress Bar
import VueProgressBar from 'vue-progressbar'
const options = {
    color: '#bffaf3',
    failedColor: '#874b4b',
    thickness: '5px',
    transition: {
        speed: '0.2s',
        opacity: '0.6s',
        termination: 300
    },
    autoRevert: true,
    location: 'top',
    inverse: false
};

Vue.use(VueProgressBar, options);
// load data after action
window.Fire = new Vue();

// Sweet Alert used for alert
import swal from 'sweetalert2';
window.swal = swal;
const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
window.toast = toast;
// Using Moment js for formating time
import moment from 'moment'


// Vue Js form validation
import {
    Form,
    HasError,
    AlertError
} from 'vform'
window.Form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
// component using pagination
Vue.component('pagination', require('laravel-vue-pagination'));

// used for vue routing
import VueRouter from 'vue-router'
Vue.use(VueRouter);


// route Link
let routes = [{
        path: '/dashboard',
        component: require('./components/DashboardComponent.vue')
    },
    {
        path: '/developer',
        component: require('./components/DeveloperComponent.vue')
    },
    {
        path: '/profile',
        component: require('./components/ProfileComponent.vue')
    },
    {
        path: '/users',
        component: require('./components/UsersComponent.vue')
    },
    // Make sure it's the end of all other component
    {
        path: '*',
        component: require('./components/NotFoundComponent.vue')
    }
];

// router instance
const router = new VueRouter({
    routes, // short for `routes: routes`
    mode: 'history',

});
// Vue Filter Using Globally
Vue.filter('upText', function (text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
});
Vue.filter('myDate', function (created) {
    return moment(created).format('MMMM Do YYYY');
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
// Only Use for test
Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);
Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('not-found', require('./components/NotFoundComponent.vue'));

const app = new Vue({
    el: '#app',
    router,
    data:{
        search: ''
    },
    methods: {
        searchit:_.debounce(()=>{
            Fire.$emit('searching');
        },1000)

    }

});

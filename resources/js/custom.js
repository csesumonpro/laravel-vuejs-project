export default class Gate {
    constructor(current) {
        this.user = current;
    }

    isAdmin() {
        console.log(this.user);
        return this.user.type === 'admin';
    }
    isUser() {
        return this.user.type === 'user';
    }
    isAuthor() {
        return this.user.type === 'author';
    }
    isAdminOrAuthor(){
        if (this.user.type === 'author' || this.user.type === 'admin'){
            return true;
        }

    }
}

<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('is_admin', function ($user) {
            return $user->type === 'admin';
        });
        Gate::define('is_user', function ($user) {
            return $user->type === 'user';
        });
        Gate::define('is_author', function ($user) {
            return $user->type === 'author';
        });
        Passport::routes();

        //
    }
}

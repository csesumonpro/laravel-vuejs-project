<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Image;




//Client ID: 1
//Client Secret: 0B9AUvxaSH9z0CE6oNUgGyads0BA2kKxmunEI6oe
//Password grant client created successfully.
//Client ID: 2
//Client Secret: De8oUx1EeLJDn0UsEnfsrpaquEimC39Gflc2v3io

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');

    }

    public function index()
    {
//         $this->authorize('is_admin');
        if (\Gate::allows('is_admin') || \Gate::allows('is_author') ) {
           return User::latest()->paginate(10);
        }

    }
    public function find_user(){
        $search = \Request::get('q');
            $users = User::where('type','LIKE',"%$search")
                ->orWhere('name','LIKE',"%$search%")
                    ->orWhere('email','LIKE',"%$search%")
            ->paginate(10);
        return $users;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users|max:191',
            'password' => 'required|max:19|min:6',
            'type' => 'required',
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->type = $request->type;
        $user->bio = $request->bio;
        $user->photo = $request->photo;
        $user->password = Hash::make($request->password);
        $user->save();
        return $user;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return auth('api')->user();
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_user_profile(Request $request)
    {
        $user = auth('api')->user();
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'password' => '|required|sometimes|max:19|min:6',

        ]);
        $current_image = $user->photo;
        $originalImage = $request->photo;

        if ($originalImage != $current_image) {
            $cl_pos = strpos($request->photo, ";");
            $sub = substr($request->photo, 0, $cl_pos);
            $ext = explode("/", $sub)[1];
            $name = time() . "." . $ext;
            $thumbnailImage = Image::make($originalImage);
            $originalPath = public_path() . "/img/profile/";
            $thumbnailImage->save($originalPath . $name);
            $request->merge(['photo' => $name]);
            $user_photo = $originalPath . $current_image;
            if (file_exists($user_photo)) {
                @unlink($user_photo);
            }
        }

        if ($request->password != null) {
            $request->merge(['password' => Hash::make($request['password'])]);
        }

        $user->update($request->all());
        return ['message' => "Success"];


    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'password' => 'sometimes|max:19|min:6',
            'type' => 'required',
        ]);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->type = $request->type;
        $user->bio = $request->bio;
        $user->password = Hash::make($request->password);
        $user->save();
//        $user->update($request->all());
        return ['msg' => 'User Updated Successfully'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('is_admin');
        $user = User::findOrFail($id);
        $user->delete();
        return ['message' => 'User Deleted'];
    }
}
